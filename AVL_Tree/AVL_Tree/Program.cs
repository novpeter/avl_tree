﻿using System;
using System.Reflection;
using DataStructures;

namespace AVLTree
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            /* Example
             
            var tree = new AVLTree<int, string>();
            tree.Add(10, null);
            tree.Add(5, null);
            tree.Add(20, null);
            tree.Add(40, null);
            tree.Add(30, null);
            tree.Add(1, null);
            tree.Add(7, null);
            tree.Add(50, null);
            tree.Add(45, null);
            tree.Remove(10);
            
            Console.WriteLine(tree.Contains(50));
            tree.UpdateValue(50, "New value of 50");
            Console.WriteLine(tree.GetValue(50));

            */
        }
    }
}
