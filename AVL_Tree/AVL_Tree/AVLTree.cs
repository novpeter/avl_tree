﻿using System;

namespace DataStructures
{

    public class Node<T, V> where T : IComparable      
    {
        public int Height { get; set; }
        public T Key { get; set; }
        public V Value { get; set; }
        public Node<T, V> Left { get; set; }
        public Node<T, V> Right { get; set; }
        
        public Node(T key, V value)
        {
            Key = key;
            Value = value;
            Height = 1;
        }

        // For comfortable debugging
        public override string ToString()
        {
            return $"{Key.ToString()} : {Value.ToString()}";
        }
    }
    
    public class AVLTree<T, V> where T : IComparable
    {   
        private Node<T, V> Root { get; set; }
        public int Count { get; private set; }
        
        public void Add(T key, V value)
        {
            if (key == null || value == null)
                throw new ArgumentNullException();
            if (Root == null)
            {
                Root = new Node<T, V>(key, value);
                Count++;
            }
            else
            { 
                Root = InsertRecursively(Root, key, value); 
            }
        }
            
        private Node<T, V> InsertRecursively(Node<T, V> currentRoot,T key, V value)
        {
            /*  1. Perform the normal BST insertion  */
            if (currentRoot == null)
            {
                Count++;
                return new Node<T, V>(key, value);
            }
            
            if (key.CompareTo(currentRoot.Key) < 0)
            {
                currentRoot.Left = InsertRecursively(currentRoot.Left, key, value);
            }
            else if (key.CompareTo(currentRoot.Key) > 0)
            {
                currentRoot.Right = InsertRecursively(currentRoot.Right, key, value);
            }
            else
            {
                // Duplicate keys not allowed
                if (!currentRoot.Value.Equals(value))
                    currentRoot.Value = value;
                return currentRoot;
            }
            
            /*  2. Update height of this ancestor node */
            currentRoot.Height = 1 + Math.Max(Height(currentRoot.Left), Height(currentRoot.Right));
            
            /* 
                3. Get the balance factor of this ancestor
                node to check whether this node became unbalanced 
            */
            int balance = GetBalance(currentRoot);
            
            /* If this node becomes unbalanced, then there are 4 cases */
            // Left Left Case
            if (balance > 1 && key.CompareTo(currentRoot.Left.Key) < 0)
                return RightRotate(currentRoot);
    
            // Right Right Case
            if (balance < -1 && key.CompareTo(currentRoot.Right.Key) > 0)
                return LeftRotate(currentRoot);
    
            // Left Right Case
            if (balance > 1 && key.CompareTo(currentRoot.Left.Key) > 0) 
            {
                currentRoot.Left = LeftRotate(currentRoot.Left);
                return RightRotate(currentRoot);
            }
    
            // Right Left Case
            if (balance < -1 && key.CompareTo(currentRoot.Right.Key) < 0) 
            {
                currentRoot.Right = RightRotate(currentRoot.Right);
                return LeftRotate(currentRoot);
            }
            
            /* return the (unchanged) node pointer */
            return currentRoot;
        }
        
        /* A utility function to right rotate subtree rooted with y */
        private Node<T, V> RightRotate(Node<T, V> y) {
            Node<T, V> x = y.Left;
            Node<T, V> T2 = x.Right;
    
            // Perform rotation
            x.Right = y;
            y.Left = T2;
    
            // Update heights
            y.Height = Math.Max(Height(y.Left), Height(y.Right)) + 1;
            x.Height = Math.Max(Height(x.Left), Height(x.Right)) + 1;
    
            // Return new root
            return x;
        }
    
        /* A utility function to left rotate subtree rooted with x */
        private Node<T, V> LeftRotate(Node<T, V> x) 
        {
            Node<T, V> y = x.Right;
            Node<T, V> T2 = y.Left;
    
            // Perform rotation
            y.Left = x;
            x.Right = T2;
    
            // Update heights
            x.Height = Math.Max(Height(x.Left), Height(x.Right)) + 1;
            y.Height = Math.Max(Height(y.Left), Height(y.Right)) + 1;
    
            // Return new root
            return y;
        }

        public void Remove(T key)
        {
            if (GetNode(key) == null)
                throw new Exception($"There is no object with key {key}");
            DeleteNode(Root, key);   
        }

        private Node<T, V> DeleteNode(Node<T, V> currentRoot, T key)
        { 
            // STEP 1: PERFORM STANDARD BST DELETE
            if (currentRoot == null)
                return currentRoot;

            // If the key to be deleted is smaller than
            // the root's key, then it lies in left subtree
            if (key.CompareTo(currentRoot.Key) < 0)
                currentRoot.Left = DeleteNode(currentRoot.Left, key);

            // If the key to be deleted is greater than the
            // root's key, then it lies in right subtree
            else if (key.CompareTo(currentRoot.Key) > 0)
                currentRoot.Right = DeleteNode(currentRoot.Right, key);

            // if key is same as root's key, then this is the node to be deleted
            else
            {
                Count--;
                
                // node with only one child or no child
                if ((currentRoot.Left == null) || (currentRoot.Right == null))
                {
                    Node<T, V> temp = null;
                    if (temp == currentRoot.Left)
                        temp = currentRoot.Right;
                    else
                        temp = currentRoot.Left;

                    // No child case
                    if (temp == null)
                    {
                        temp = currentRoot;
                        currentRoot = null;
                    }
                    // One child case
                    else
                        // Copy the contents of
                        currentRoot = temp;
                }
                // the non-empty child
                else
                {
                    // node with two children: Get the inorder
                    // successor (smallest in the right subtree)
                    Node<T, V> temp = GetInorderSuccessor(currentRoot.Right);

                    // Copy the inorder successor's data to this node
                    currentRoot.Key = temp.Key;     
                    currentRoot.Value = temp.Value;
                    
                    // Delete the inorder successor
                    currentRoot.Right = DeleteNode(currentRoot.Right, temp.Key);
                }
            }
            
            // If the tree had only one node then return
            if (currentRoot == null)
                return currentRoot;
    
            // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
            currentRoot.Height = Math.Max(Height(currentRoot.Left), Height(currentRoot.Right)) + 1;
    
            // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
            //  this node became unbalanced)
            int balance = GetBalance(currentRoot);
    
            // If this node becomes unbalanced, then there are 4 cases
            // Left Left Case
            if (balance > 1 && GetBalance(currentRoot.Left) >= 0)
                return RightRotate(currentRoot);
    
            // Left Right Case
            if (balance > 1 && GetBalance(currentRoot.Left) < 0) {
                currentRoot.Left = LeftRotate(currentRoot.Left);
                return RightRotate(currentRoot);
            }
    
            // Right Right Case
            if (balance < -1 && GetBalance(currentRoot.Right) <= 0)
                return LeftRotate(currentRoot);
    
            // Right Left Case
            if (balance < -1 && GetBalance(currentRoot.Right) > 0) {
                currentRoot.Right = RightRotate(currentRoot.Right);
                return LeftRotate(currentRoot);
            }
            
            return currentRoot;
        }

        public V GetValue(T key)
        {
            Node<T, V> tmp = GetNode(key);
            if (tmp == null)
                throw new Exception($"Key '{key}' is not represented in the tree");
            return tmp.Value;
        }

        public void UpdateValue(T key, V value)
        {
            Node<T, V> tmp = GetNode(key);
            if (tmp == null)
                throw new Exception($"Key '{key}' is not represented in the tree");
            tmp.Value = value;
        }

        public bool Contains(T key) => GetNode(key) != null;
        
        private Node<T, V> GetNode(T key)
        {
            if (key == null) throw new ArgumentNullException();
            Node<T, V> current = Root;
            while (current != null) 
            {
                if (key.CompareTo(current.Key) == 0)
                    return current;
                else if (key.CompareTo(current.Key) < 0)
                    current = current.Left;
                else
                    current = current.Right; 
            }
            return null;
        }
        
        private Node<T, V> GetInorderSuccessor(Node<T, V> node) 
        {
            Node<T, V> current = node;
            // loop down to find the leftmost leaf 
            while (current.Left != null)
                current = current.Left;
    
            return current;
        }

        // Get Balance factor of node N
        private int GetBalance(Node<T, V> node) => node == null ? 0 : Height(node.Left) - Height(node.Right);

        // Get height of the node
        private int Height(Node<T, V> node) => node == null ? 0 : node.Height;
    }
}