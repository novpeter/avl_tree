﻿using NUnit.Framework;
using System;
using System.Reflection;

namespace DataStructures
{
    [TestFixture]
    public class AVLTreeTests 
    {     
        [Test]
        public void AddWithoutUnbalancing()
        {
            var tree = CreateTree(new int[] { 10, 5, 20, 15 });
            tree.Add(25, 25);
            tree.Add(1, 1);
            tree.Add(7, 7);
            
            Assert.AreEqual(tree.Contains(25), true);
            Assert.AreEqual(tree.Contains(1), true);
            Assert.AreEqual(tree.Contains(7), true);
            
            var root = GetRoot(tree);
            
            Assert.AreEqual(1, root.Left.Left.Value);
            Assert.AreEqual(7, root.Left.Right.Value);
            Assert.AreEqual(15, root.Right.Left.Value);
            Assert.AreEqual(25, root.Right.Right.Value);
        }
        
        /*
            For comfortable vizualization of insert cases
            go to 'TestCases' folder there is the pdf file 
            with 4 cases is located.  
        */
        [Test]
        public void AddLeftLeftCase()
        {
            var tree = CreateTree(new int[] { 10, 5, 15, -10 });
            tree.Add(-20, -20);
            var root = GetRoot(tree);
            Assert.AreEqual(tree.Contains(-20), true);
            Assert.AreEqual(-10, root.Left.Value);
            Assert.AreEqual(-20, root.Left.Left.Value);
            Assert.AreEqual(5, root.Left.Right.Value);
        }  
        
        [Test]
        public void AddLeftRightCase()
        {
            var tree = CreateTree(new int[] { 10, 5, 15, -10 });
            tree.Add(-5, -5);
            var root = GetRoot(tree);
            Assert.AreEqual(tree.Contains(-5), true);
            Assert.AreEqual(-5, root.Left.Value);
            Assert.AreEqual(-10, root.Left.Left.Value);
            Assert.AreEqual(5, root.Left.Right.Value);
        } 
        
        [Test]
        public void AddRightRightCase()
        {
            var tree = CreateTree(new int[] { 10, 5, 20, 30 });
            tree.Add(40, 40);
            var root = GetRoot(tree);
            Assert.AreEqual(tree.Contains(40), true);
            Assert.AreEqual(30, root.Right.Value);
            Assert.AreEqual(20, root.Right.Left.Value);
            Assert.AreEqual(40, root.Right.Right.Value);
        }
        
        [Test]
        public void AddRightLeftCase()
        {
            var tree = CreateTree(new int[] { 10, 5, 20, 30 });
            tree.Add(25, 25);
            var root = GetRoot(tree);
            Assert.AreEqual(tree.Contains(25), true);
            Assert.AreEqual(25, root.Right.Value);
            Assert.AreEqual(20, root.Right.Left.Value);
            Assert.AreEqual(30, root.Right.Right.Value);
        }

        [Test]
        public void RemoveRoot()
        { 
            var tree = CreateTree(new int[] { 10, 5, 20, 30 , 1, 7, 15, 25, 17 });
            var root = GetRoot(tree);
            Assert.AreEqual(10, root.Key);
            tree.Remove(10);
            Assert.AreEqual(tree.Contains(10), false);
            Assert.AreEqual(15, root.Key);
            Assert.AreEqual(17, root.Right.Left.Key);
        }

        [Test]
        public void RemoveLeafs()
        { 
            var tree = CreateTree(new int[] { 10, 5, 20, 30 , 1, 7, 15, 25, 17 });
            var root = GetRoot(tree);
            Assert.AreEqual(10, root.Key);
            tree.Remove(17);
            tree.Remove(1);
            Assert.AreEqual(tree.Contains(17), false);
            Assert.AreEqual(tree.Contains(1), false);
        }
        
        [Test]
        public void RemoveWithRebelance()
        { 
            var tree = CreateTree(new int[] { 10, 5, 20, 1, 7, 15, 25, 17 });
            var root = GetRoot(tree);
            tree.Remove(20);
            Assert.AreEqual(17, root.Right.Key);
            Assert.AreEqual(15, root.Right.Left.Key);
            Assert.AreEqual(25, root.Right.Right.Key);
        }
        
        [Test]
        public void UpdateValue1()
        { 
            var tree = CreateTree(new int[] { 10, 5, 20, 1, 7, 15, 25, 17 });
            Assert.AreEqual(1, tree.GetValue(1));
            Assert.AreEqual(10, tree.GetValue(10));
            tree.UpdateValue(1, 100);
            tree.Add(10, -10);
            Assert.AreEqual(100, tree.GetValue(1));
            Assert.AreEqual(-10, tree.GetValue(10));
        }
        
        [Test]
        public void Exceptions()
        { 
            var tree = CreateTree(new int[] { 10, 5, 20, 1, 7, 15, 25, 17 });
            Assert.Throws<Exception>(() => tree.GetValue(55));
            Assert.Throws<Exception>(() => tree.UpdateValue(55, 55));
            Assert.Throws<Exception>(() => tree.Remove(55));
        }
        
        private AVLTree<int, int> CreateTree(int[] keys)
        {
            var tree = new AVLTree<int, int>();
            foreach (var key in keys)
                tree.Add(key, key);
            return tree;
        }

        private Node<int, int> GetRoot(AVLTree<int, int> tree)
        { 
            var prop = typeof(AVLTree<int, int>).GetProperty("Root", BindingFlags.NonPublic | BindingFlags.Instance);
            return (Node<int, int>)prop.GetValue(tree);
        }
    }
}
